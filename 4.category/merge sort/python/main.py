def mergeSort(thisIsMyFavoriteArray): 
    if len(thisIsMyFavoriteArray) >1: 
        mid = len(thisIsMyFavoriteArray)//2 
        left = thisIsMyFavoriteArray[:mid] 
        right = thisIsMyFavoriteArray[mid:] 
  
        mergeSort(left)  
        mergeSort(right)  
  
        indexLeft = indexRight = indexBoth = 0
          
        while indexLeft < len(left) and indexRight < len(left): 
            if left[indexLeft] < right[indexRight]: 
                thisIsMyFavoriteArray[indexBoth] = left[indexLeft] 
                indexLeft+=1
            else: 
                thisIsMyFavoriteArray[indexBoth] = right[indexRight] 
                indexRight+=1
            indexBoth+=1
          
        while indexLeft < len(left): 
            thisIsMyFavoriteArray[indexBoth] = left[indexLeft] 
            indexLeft+=1
            indexBoth+=1
          
        while indexRight < len(right): 
            thisIsMyFavoriteArray[indexBoth] = right[indexRight] 
            indexRight+=1
            indexBoth+=1
  

if __name__ == '__main__': 
    thisIsMyFavoriteArray = [12, 11, 13, 5, 6, 7]  
    print ("Given array is",thisIsMyFavoriteArray)  
    mergeSort(thisIsMyFavoriteArray) 
    print("Sorted array is: ", thisIsMyFavoriteArray) 
