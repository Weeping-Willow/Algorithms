def quickSort(someArray,low,high): 
    if low < high: 
        partioned = partition(someArray,low,high) 
 
        quickSort(someArray, low, partioned-1) 
        quickSort(someArray, partioned+1, high) 

def partition(someArray,low,high): 
    index = ( low-1 )         
    pivot = someArray[high]     
  
    for loopIndex in range(low , high): 
  
        if   someArray[loopIndex] <= pivot: 
            index = index+1 
            someArray[index],someArray[loopIndex] = someArray[loopIndex],someArray[index] 
  
    someArray[index+1],someArray[high] = someArray[high],someArray[index+1] 
    return ( index+1 ) 


veryCoolArray = [10, 7, 8, 9, 1, 5] 
arrayLength = len(veryCoolArray) 
quickSort(veryCoolArray,0,arrayLength-1) 
print ("Sorted array is:",veryCoolArray) 
