import hashlib

def hashWithSh1(thisString):
    hasedString = hashlib.sha1(thisString)
    return hasedString

def hashWithMd5(thisString):
    hasedString = hashlib.md5(thisString)
    return hasedString

hashWithSh1 =  hashlib.sha1(b'sapuvisAbols').hexdigest()
hashWithMd5 = hashlib.md5(b'sapuvisAbols').hexdigest()

print("sha1 hash of sapuvisAbols is ", hashWithSh1)
print("Md5 hash of sapuvisAbols is ", hashWithMd5)
