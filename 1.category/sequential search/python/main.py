def search(dati,vertiba):
    reizes = 0
    for x in dati:
        reizes=reizes+1
        if x==vertiba:
            print ("Meklētā vērtība ",vertiba," Operācijas ",reizes," Indeks pozicija ",reizes-1)
            return reizes-1
    print("Nav atrasts")
    return None

def test(dati):
    search(dati,5) #6 operacijas un 5 indeks
    search(dati,1) #2 operacijas un 1 indeks
    search(dati,3) #4 operacijas un 3 indeks
    search(dati,32) #33 operacijas un 32 indeks
    search(dati,63) #64 operacijas un 63 indeks
    search(dati,53) #54 operacijas un 53 indeks
    search(dati,84) #85 operacijas un 84 indeks
    search(dati,-4) #Nav
    search(dati,100) #Nav
    search(dati,0) #1 operacijas un 0 indeks

x = 0
dati = []
while x < 100:
    dati.append(x)
    x = x + 1
test(dati)
