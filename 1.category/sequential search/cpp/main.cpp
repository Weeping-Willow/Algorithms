#include <iostream>
#include <vector>

int search(std::vector<int> dati, int vertiba)
{
    for (int i = 0; i < dati.size(); i++)
    {
        if (dati.at(i) == vertiba) 
        {
            std::cout << "Meklētā vērtība " << vertiba << " Operācijas " << i+1 << " Indeks pozicija " << i << std::endl;
            return i;
        }
    }
    std::cout << "Nav atrasts";
    return NULL;
}
void test(std::vector<int> dati)
{
    search(dati, 5); //6 operacijas un 5 indeks
    search(dati, 1); //2 operacijas un 1 indeks
    search(dati, 3); //4 operacijas un 3 indeks
    search(dati, 32); //33 operacijas un 32 indeks
    search(dati, 63); //64 operacijas un 63 indeks
    search(dati, 53); //54 operacijas un 53 indeks
    search(dati, 84); //85 operacijas un 84 indeks
    search(dati, -4); //Nav
    search(dati, 100); //Nav
    search(dati, 0); //1 operacijas un 0 indeks
}

int main() {
    std::vector<int> dati;
    for (int i = 0; i < 100; i++)
        dati.push_back(i);

    test(dati);
}
