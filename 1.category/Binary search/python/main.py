def binary_search(dati, vertiba):
    low = 0
    high = len(dati)-1
    reizes = 0
    while low <= high:
        mid = int((low + high) / 2)
        guess = dati[mid]
        reizes +=1
        if guess == vertiba:
            print ("Meklētā vērtība ",vertiba," Operācijas ",reizes," Indeks pozicija ",mid)
            return mid
        elif vertiba < guess:
            high = mid - 1
        else:
            low = mid + 1
    print ("Nav atrasts")
    return None

def test(dati):
    binary_search(dati,5) #4 operacijas un 5 indeks
    binary_search(dati,1) #7 operacijas un 1 indeks
    binary_search(dati,3) #6 operacijas un 3 indeks
    binary_search(dati,32) #7 operacijas un 32 indeks
    binary_search(dati,63) #7 operacijas un 63 indeks
    binary_search(dati,53) #6 operacijas un 53 indeks
    binary_search(dati,84) #7 operacijas un 84 indeks
    binary_search(dati,-4) #Nav
    binary_search(dati,100) #Nav
    binary_search(dati,0) #6 operacijas un 0 indeks

x = 0
dati = []
while x < 100:
    dati.append(x)
    x = x + 1
test(dati)
