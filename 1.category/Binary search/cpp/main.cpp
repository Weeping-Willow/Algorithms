#include <iostream>
#include <vector>

int binary_search(std::vector<int> dati,int vertiba)
  {
    int low = 0;
    int high = dati.size()-1;
    int reizes = 0;

    while (low <= high){

        int mid = (low + high) / 2;
        int guess = dati.at(mid);
        
        reizes++;

        if (guess == vertiba){

           std::cout<<"Meklētā vērtība "<<vertiba<<" Operācijas "<<reizes<<" Indeks pozicija "<<guess<<std::endl;

            return mid;
        }
        else if(vertiba < guess){
            high = mid - 1;
        }
        else
            low = mid + 1;
    }

    std::cout<<"Nav atrasts"<<std::endl;
    return NULL;
  }
void test(std::vector<int> dati)
{
  binary_search(dati,5); //4 operacijas un 5 indeks
  binary_search(dati,1); //7 operacijas un 1 indeks
  binary_search(dati,3); //6 operacijas un 3 indeks
  binary_search(dati,32); //7 operacijas un 32 indeks
  binary_search(dati,63); //7 operacijas un 63 indeks
  binary_search(dati,53); //6 operacijas un 53 indeks
  binary_search(dati,84); //7 operacijas un 84 indeks
  binary_search(dati,-4); //Nav
  binary_search(dati,100); //Nav
  binary_search(dati,0); //6 operacijas un 0 indeks
}
int main() {
  std::vector<int> dati; 
  for (int i = 0; i < 100; i++) 
        dati.push_back(i); 
  
  test(dati);
}
