class Main {
  public static void main(String[] args) {
    System.out.println(printHelloXTimes(5));
  }

  public static String printHelloXTimes(int times)
    {
        if(times == 0)
        {
            return "";
        }
        else
        {
            return "Hello " + printHelloXTimes(times-1);
        }
        
    }
}