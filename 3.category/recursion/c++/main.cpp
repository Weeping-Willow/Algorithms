#include <iostream>

int factorial(int n) {
    if (n <= 1)
        return 1;
    else
        return n * factorial(n - 1);
}
int main() {
    int number;
    std::cout << "Enter a number: ";
    std::cin >> number;
    std::cout << "Factorial of entered number: " << factorial(number);
    return 0;
}