import queue
que = queue.LifoQueue()

for x in range(4):
    que.put(x)

while not que.empty():
    print(que.get(), " ")
