import random
from random import seed
from random import randint

class Node:
    
    def __init__(self, dataval=None):
        self.dataval = dataval
        self.nextval = None

class SLinkedList:
    
    def __init__(self):
        self.headval = None
        self.sunu = None
    def listprint(self):
        printval = self.headval
        while printval is not None:
            print (printval.dataval)
            printval = printval.nextval
    
    def selectionSort(self):
        list2 = SLinkedList()
        list2.headval = Node(0)

        e1 = Node(0)
        list2.headval.nextval = e1
        x = 0
        while self.headval is not None:
            
            smallest = self.findSmallest()
            self.delete(smallest)
            if x == 0:
                list2.headval = Node(self.sunu)
                x = x+1
            elif x == 1:
                e1 = Node(self.sunu)
                list2.headval.nextval = e1
                x = x+1
            else:
                e2= Node(self.sunu)
                e1.nextval = e2
                e1 = e2
                x = x+1
        return list2
    
    def findSmallest(self):
        if self.headval is None:
            return
        else:
            smallest = self.headval.dataval
            printval = self.headval
            i = 0
            smallest_index = i
            while printval is not None:
                if printval.dataval < smallest:
                    smallest = printval.dataval
                    smallest_index = i
                printval = printval.nextval
                i += 1
            self.sunu = smallest
            return smallest_index

    def delete(self,index):
        if self.headval == None: 
            return

        temp  = self.headval

        if index == 0:
            self.headval = temp.nextval
            temp = None
            return
        
        for i in range(index - 1):
            temp =  temp.nextval
            if temp is None:
                break
        if temp is None:
            return
        if temp.nextval is None:
            return
        nextval = temp.nextval.nextval
        temp.nextval = None
        temp.nextval = nextval



list1 = SLinkedList()
list1.headval = Node(randint(0, 20))


x = 2

seed(1)
e1 = Node(randint(0, 20))
list1.headval.nextval = e1

while x < 25:
  e2= Node(randint(0, 20))
  e1.nextval = e2
  e1 = e2
  x = x+1
list1.listprint()
list1 = list1.selectionSort()
print("Printed list")
list1.listprint()
