Algorithms
  # The big O:

      • Speed is measured in iteration rather than seconds


      • The big O is the worst-case scenario


  # Array:

      • All your elements are stored right next to each other


      • Slow inserts and deletes because all of the array might have to be moved


      • All elements should be the same in an array 


      • Allow fast reads because of random access(array saves the location of the first address and type, the rest is calculated thanks to 1st point ).


  # Linked list:

      • Elements are all over the place, and one element stores the address of the next one.


      • Linked lists allow fast inserts and deletes.


      • Reads are slow because only sequential access is available


  # Binary Search:

      • Decreases the possible answer candidates by half every iteration


      • Binary search is a lot faster than a simple search(sequential search)


      • O(log2 X = N) is faster than O(n), but it gets a lot faster once the list of items you’re searching through grows


      • Array or list must be sorted in ascending or descending order for it to work



  # Selection sort:

      • Checking fewer elements each time it makes the array smaller each run (it does it by removing the highest or smallest value)
      
      • The big O for this is O(n^2)
      
  # Quicksort
  
    • The big O is O(n log n)
    
    • The big O is the same as in merge sort but on average, it's faster
    
    • The speed is dependent on a pivot(a random number)
    
    • quicksort is easier with recursion
    
    • You chose a pivot and then the numbers are compared and divided into 2 arrays one with bigger values and one with smaller. Then you call the function again on these 2 arrays.
    
    • For the sort to be quicker have to choose a pivot that divides numbers as equally as possible(into 2 arrays)
    
    
  # Recursion
    
    • Calling the function from the same function 
    
    • Uses the stack which kinda is like a list of tasks
    
    • Fifo and Lifo or (First in first out) our Last in First out
    
  # Search and destroy
  
    • A way to come up with an algorithm

    • You do that by finding the base case

    •The base case is 1. the easiest solution or 2.reduce the problem until you got the first point
